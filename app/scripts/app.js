'use strict';

/**
 * @ngdoc overview
 * @name adminLeadApp
 * @description
 * # adminLeadApp
 *
 * Main module of the application.
 */
var app = angular
    .module('adminLeadApp', [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'pascalprecht.translate',
        'flash',
        'angular-toArrayFilter',
        'ui.bootstrap',
        'ch.filters',
        'ngFileUpload'
    ])
    .config(function ($httpProvider, $routeProvider, $translateProvider, config) {
        //Append CSRF Token To Request heades
        $httpProvider.defaults.headers.common['lead-api'] = config.api.key;

        //config translate module
        var lang_path = 'languages/';
        $translateProvider.useStaticFilesLoader({
            prefix: lang_path,
            suffix: '.json'
        });
        $translateProvider.useSanitizeValueStrategy('escaped');
        $translateProvider.preferredLanguage('en_US');

        $routeProvider
            .when('/', {
                title: 'dashboard',
                templateUrl: 'views/main.html',
                controller: 'MainController',
                controllerAs: 'main'
            })
            .when('/all_users', {
                title: 'all_users',
                templateUrl: 'views/all_users.html',
                controller: 'UsersController',
                controllerAs: 'users'
            })
            .when('/edit_user/:userId', {
                title: 'edit_user',
                templateUrl: 'views/edit_user.html',
                controller: 'UsersController',
                controllerAs: 'users'
            })
            .when('/meta_objects', {
                title: 'meta_objects_list',
                templateUrl: 'views/meta_objects_list.html',
                controller: 'MetaObjectsController',
                controllerAs: 'meta_objects'
            })
            .when('/meta_objects/edit/:id', {
                title: 'edit_meta_object',
                templateUrl: 'views/meta_objects_edit.html',
                controller: 'MetaObjectsController',
            })
            .when('/xbpmn_notations', {
                title: 'xbpmn_notations_list',
                templateUrl: 'views/xbpmn_notations_list.html',
                controller: 'XbpmnNotationsController',
                controllerAs: 'xbpmn_notations'
            })
            .when('/xbpmn_notations/add', {
                title: 'add_xbpmn_notations',
                templateUrl: 'views/xbpmn_notations_edit.html',
                controller: 'XbpmnNotationsController',
                controllerAs: 'add_xbpmn_notations'
            })
            .when('/xbpmn_notations/edit/:id', {
                title: 'edit_xbpmn_notations',
                templateUrl: 'views/xbpmn_notations_edit.html',
                controller: 'XbpmnNotationsController',
                controllerAs: 'edit_xbpmn_notations'
            })
            .when('/business_model_notations', {
                title: 'business_model_notations_list',
                templateUrl: 'views/business_model_notations_list.html',
                controller: 'BusinessModelNotationsController',
                controllerAs: 'business_model_notations'
            })
            .when('/business_model_notations/add', {
                title: 'add_business_model_notations',
                templateUrl: 'views/business_model_notations_edit.html',
                controller: 'BusinessModelNotationsController',
                controllerAs: 'add_business_model_notations'
            })
            .when('/business_model_notations/edit/:id', {
                title: 'edit_business_model_notations',
                templateUrl: 'views/business_model_notations_edit.html',
                controller: 'BusinessModelNotationsController',
                controllerAs: 'edit_business_model_notations'
            })
            .when('/social_media_notations', {
                title: 'social_media_notations_list',
                templateUrl: 'views/social_media_notations_list.html',
                controller: 'SocialMediaNotationsController',
                controllerAs: 'social_media_notations'
            })
            .when('/social_media_notations/add', {
                title: 'add_social_media_notation',
                templateUrl: 'views/social_media_notations_edit.html',
                controller: 'SocialMediaNotationsController',
                controllerAs: 'add_social_media_notation'
            })
            .when('/social_media_notations/edit/:id', {
                title: 'edit_social_media_notation',
                templateUrl: 'views/social_media_notations_edit.html',
                controller: 'SocialMediaNotationsController',
                controllerAs: 'edit_social_media_notation'
            })
            .otherwise({
                redirectTo: '/'
            });
    }).run(['$location', '$rootScope', '$translate', function ($location, $rootScope, $translate) {
        $rootScope.$on('$routeChangeSuccess', function (event, current) {

            if (current.hasOwnProperty('$$route')) {

                $rootScope.leadadmin_title = $translate.instant('site_name') + ' | ' + $translate.instant(current.$$route.title);
            }
        });
    }]);


