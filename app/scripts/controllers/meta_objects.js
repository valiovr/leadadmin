/**
 * Created by Miroslav Marinov on 24.09.2015.
 */

'use strict';

/**
 * @ngdoc function
 * @name adminLeadApp.controller:MetaObjectsController
 * @description
 * # MetaObjectsController
 * Controller of the adminLeadApp

 */

app.controller('MetaObjectsController', function ($scope, ApiService, config, $routeParams, $filter, $translate, Upload, Flash){

    $scope.metaObjects = [];
    $scope.front_url = config.front_url;
    $scope.pageLoading = false;
    $scope.singleMetaObject = {};

    /**
     * Init Meta Objects List Default data
     */
    var init = function () {
        //check is edit page or  list page
        if (typeof $routeParams.id == 'undefined') {
            //on list page make singlw
            $scope.singleMetaObject = {};
            getMetaObjects();
        } else {
            getMetaObjectByid($routeParams.id);
        }
    };
    /**
     * Get Meta Objects List
     */
    var getMetaObjects = function () {
        $scope.pageLoading = true;
        ApiService.get(config.api.url + '/' + 'admin/get_meta_objects').then(function (data, status, headers, config) {
            $scope.metaObjects = data;
            $scope.pageLoading = false;
        }, function (data, status, headers, config) {
            if (status == 401) {
                window.location.href = '/';
            }
        });
    };

    /**
     * Get Meta Object by ID
     */
    var getMetaObjectByid = function (meta_object_id) {

        ApiService.get(config.api.url + '/' + 'admin/get_meta_object_by_id/' + meta_object_id).then(function (data, status, headers, config) {
            $scope.singleMetaObject = data;

        }, function (data, status, headers, config) {
            if (status == 401) {
                window.location.href = '/';
            }
        });

    };

    /**
     * Update Meta Object Without change notation
     */
    var saveWithoutImage = function () {
        ApiService.post(config.api.url + '/' + 'admin/save_meta_object', $scope.singleMetaObject).then(function (data, status, headers, config) {
            Flash.create('success', $translate.instant('meta_object_success_updated'), 'formMessage');
            window.location.href = '#/meta_objects';
        }, function (data, status, headers, config) {
            window.location.href = '/';
        });
    };

    /**
     * Update Meta Object and change notation
     */
    var saveAndUpload = function () {

        Upload.upload({
            url: config.api.url + '/' + 'admin/save_meta_object_with_notation',
            fields: $scope.singleMetaObject,
            file: $scope.file
        }).success(function (data, status, headers, config) {
            if(typeof data.success != 'undefined'){
                Flash.create('success', $translate.instant('meta_object_success_updated'), 'formMessage');
                window.location.href = '#/meta_objects';
            }
            if(typeof data.error != 'undefined' && data.error=='meta_object_notation_upload_error'){
                Flash.create('danger', $translate.instant('meta_object_notation_upload_error'), 'formMessage');
            }
            if(typeof data.error != 'undefined' && data.error=='meta_object_not_updated'){
                Flash.create('danger', $translate.instant('meta_object_update_error'), 'formMessage');
            }
        }).error(function (data, status, headers, config) {
            Flash.create('danger', $translate.instant('meta_object_notation_upload_error'), 'formMessage');
        });
    };

    $scope.save = function () {
        if (typeof $scope.file == 'undefined') {
            saveWithoutImage();
        } else {
                saveAndUpload();
        }
    };


    //Init Default Data
    init();

});