/**
 * Created by valio on 29.9.2015 ?..
 */

'use strict';

/**
 * @ngdoc function
 * @name adminLeadApp.controller:BusinessModelNotationsController
 * @description
 * # BusinessModelNotationsController
 * Controller of the adminLeadApp
 */

/**
 * Get List
 */
app.controller('BusinessModelNotationsController', function ($scope, ApiService, config, $location, $filter, $translate, Upload, Flash) {
    $scope.businessModelNotations = [];
    $scope.front_url = config.front_url;
    $scope.singleBusinessModelNotation = {};
    $scope.pageLoading = false;
    $scope.InnerPageTitle = null;
    $scope.businessModelNotationsObjectCategories = [];
    /**
     * Init Business Model Notations List Default data
     */
    var init = function () {
        //get lo
        var path = $location.path().split("/");

        if (path[path.length - 1] == 'add') {
            $scope.InnerPageTitle = $translate.instant('add_business_model_notation');
            $scope.singleBusinessModelNotation = {};
            getBusinessModelNotationsObjectCategories();
        } else if (path[path.length - 2] == 'edit') {
            $scope.InnerPageTitle = $translate.instant('edit_business_model_notation');

            getBusinessModelNotationById(path[path.length - 1]);
            getBusinessModelNotationsObjectCategories();
        } else {
            $scope.InnerPageTitle = null;
            getBusinessModelNotations();
        }
    };
    var getBusinessModelNotations = function () {

        $scope.pageLoading = true;
        ApiService.get(config.api.url + '/' + 'admin/get_business_model_notations').then(function (data, status, headers, config) {
            $scope.businessModelNotations = data;
            $scope.pageLoading = false;
        }, function (data, status, headers, config) {
            if (status == 401) {
                window.location.href = '/';
            }
        });
    };

    var getBusinessModelNotationById = function (business_model_notation_id) {
        $scope.pageLoading = true;
        ApiService.get(config.api.url + '/' + 'admin/get_business_model_notation_by_id/' + business_model_notation_id).then(function (data, status, headers, config) {
            $scope.singleBusinessModelNotation = data;
            $scope.pageLoading = false;
        }, function (data, status, headers, config) {
            if (status == 401) {
                window.location.href = '/';
            }
        });

    };

    var getBusinessModelNotationsObjectCategories = function () {
        ApiService.get(config.api.url + '/' + 'admin/get_business_model_notations_object_categories').then(function (data, status, headers, config) {
            $scope.businessModelNotationsObjectCategories = data;
        }, function (data, status, headers, config) {
            if (status == 401) {
                window.location.href = '/';
            }
        });
    };

    var addBusinessModelNotation = function(){

        Upload.upload({
            url: config.api.url + '/' + 'admin/add_business_model_notation',
            fields: $scope.singleBusinessModelNotation,
            file: $scope.file
        }).success(function (data, status, headers, config) {
            if(typeof data.success != 'undefined'){
                Flash.create('success', $translate.instant('business_model_notation_success_added'), 'formMessage');
                window.location.href = '#/business_model_notations';
            }
            if(typeof data.error != 'undefined' && data.error=='meta_object_notation_upload_error'){
                Flash.create('danger', $translate.instant('business_model_notation_upload_error'), 'formMessage');
            }
            if(typeof data.error != 'undefined' && data.error=='meta_object_not_updated'){
                Flash.create('danger', $translate.instant('business_model_notation_update_error'), 'formMessage');
            }
        }).error(function (data, status, headers, config) {
            Flash.create('danger', $translate.instant('business_model_notation_upload_error'), 'formMessage');
        });
    };

    var editBusinessModelNotation = function () {

        if (typeof $scope.file == 'undefined') {
            ApiService.post(config.api.url + '/' + 'admin/edit_business_model_notation', $scope.singleBusinessModelNotation).then(function (data, status, headers, config) {
                Flash.create('success', $translate.instant('business_model_notation_success_updated'), 'formMessage');
                window.location.href = '#/business_model_notations';
            }, function (data, status, headers, config) {
                console.log(status);
                //window.location.href = '/';
            });
        } else {

            Upload.upload({
                url: config.api.url + '/' + 'admin/edit_business_model_notation',
                fields: $scope.singleBusinessModelNotation,
                file: $scope.file
            }).success(function (data, status, headers, config) {
                if (typeof data.success != 'undefined') {
                    Flash.create('success', $translate.instant('business_model_notation_success_updated'), 'formMessage');
                    window.location.href = '#/business_model_notations';
                }
                if (typeof data.error != 'undefined' && data.error == 'meta_object_notation_upload_error') {
                    Flash.create('danger', $translate.instant('business_model_notation_upload_error'), 'formMessage');
                }
                if (typeof data.error != 'undefined' && data.error == 'meta_object_not_updated') {
                    Flash.create('danger', $translate.instant('business_model_notation_update_error'), 'formMessage');
                }
            }).error(function (data, status, headers, config) {
                Flash.create('danger', $translate.instant('business_model_notation_upload_error'), 'formMessage');
            });
        }

    }

    $scope.save= function(){
        if(typeof $scope.singleBusinessModelNotation.id =='undefined'){
            addBusinessModelNotation();
        }else{
            editBusinessModelNotation();
        }

    };

    //Init Default Data
    init();
});
