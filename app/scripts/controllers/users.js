'use strict';

/**
 * @ngdoc function
 * @name adminLeadApp.controller:UsersController
 * @description
 * # UsersController
 * Controller of the adminLeadApp
 */
app.controller('UsersController', function ($scope, ApiService, config, $routeParams, $filter, Flash) {
        $scope.users = [];
        $scope.isReversed = false;
        $scope.sort_field = null;
        $scope.user_id = null;
        $scope.permissions = {};
        $scope.namespaces_allow = [
            { name: 'Objects', value: 5000, selected: false },
            { name: 'Matrices', value: 5001, selected: false },
            { name: 'Maps', value: 5002, selected: false },
            { name: 'Models', value: 5003,selected: false },
            { name: 'Layers', value: 5004,selected: false },
            { name: 'Enterprise Standards', value: 5005,selected: false },
            { name: 'Industry Standards', value: 5006,selected: false },
            { name: 'Plans', value: 5007, selected: false }
        ];
        $scope.namespaces_deny = [
            { name: 'Objects', value: 5000, selected: false },
            { name: 'Matrices', value: 5001, selected: false },
            { name: 'Maps', value: 5002, selected: false },
            { name: 'Models', value: 5003,selected: false },
            { name: 'Layers', value: 5004,selected: false },
            { name: 'Enterprise Standards', value: 5005,selected: false },
            { name: 'Industry Standards', value: 5006,selected: false },
            { name: 'Plans', value: 5007, selected: false }
        ];
        $scope.selection_allow = [];
        $scope.selection_deny = [];
        $scope.category_items_array=[];
        $scope.category_items_object = {};
        $scope.category_items_array_deny=[];
        $scope.category_items_object_deny = {};
        $scope.page_items_array=[];
        $scope.page_items_object = {};
        $scope.page_items_array_deny=[];
        $scope.page_items_object_deny = {};
        $scope.page_items = [];
        $scope.page_items_deny = [];
        $scope.pages_selected = [];
        $scope.pages_selected_deny = [];
        $scope.categories_selected = [];
        $scope.categories_selected_deny = [];
        $scope.category_items = [];
        $scope.category_items_deny = [];
        var init = function () {
           $scope.getAllUsers();
            if(typeof $routeParams.userId != 'undefined'){
                $scope.user_id = $routeParams.userId;
            }
        };

        /**
         * Get All users from db
         */
        $scope.getAllUsers = function () {
            ApiService.get(config.api.url + '/' + 'admin/get_all_users').then(function (data, status, headers, config) {
                $scope.users = data;
            }, function (data, status, headers, config) {

            });
        };

        /**
         * Sort users list
         * @param  string type
         */
        $scope.sort = function (type) {
            var order = 'asc';
            $scope.sort_field = type;
            if (type != $scope.sort_field && $scope.sort_field != null) {
                $scope.isReversed = false;

            }

            if ($scope.isReversed) {
                order = 'desc'
            }

            ApiService.get(config.api.url + '/' + 'admin/get_all_users' + '/' + type + '/' + order).then(function (data, status, headers, config) {
                $scope.users = data;

            }, function (data, status, headers, config) {

            });
        };

        $scope.getUserById = function (user_id) {
            ApiService.get(config.api.url + '/' + 'admin/get_user_data' + '/' + user_id).then(function (data, status, headers, config) {
                $scope.userData = data;
            }, function (data, status, headers, config) {

            });
        };

        $scope.getAllCategories = function() {

            ApiService.get(config.api.url + '/' + 'admin/get_all_categories').then(function (data, status, headers, config) {
                $scope.categories = data;
            }, function (data, status, headers, config) {

            });
        };

        $scope.getAllCategoriesDeny = function() {

            ApiService.get(config.api.url + '/' + 'admin/get_all_categories').then(function (data, status, headers, config) {
                $scope.categories_deny=data;
            }, function (data, status, headers, config) {

            });
        };

        $scope.getAllPages = function() {

            $scope.selected = '';

            ApiService.get(config.api.url + '/' + 'admin/get_all_pages').then(function (data, status, headers, config) {

                $scope.pages = data;

            }, function (data, status, headers, config) {

            });

        };

        $scope.getAllPagesDeny = function() {

            $scope.selected = '';

            ApiService.get(config.api.url + '/' + 'admin/get_all_pages').then(function (data, status, headers, config) {

                $scope.pages_deny = data;

            }, function (data, status, headers, config) {

            });

        };
        $scope.checkFuncAllow = function(id){
            var found = $scope.selection_deny.indexOf(id);
            if(found != -1){
               
              
            switch (id){
                case 5000:
                   $scope.allow5000 = false;
                   break;
                case 5001:
                   $scope.allow5001 = false;
                   break;
                case 5002:
                   $scope.allow5002 = false;
                   break;
                case 5003:
                   $scope.allow5003 = false;
                   break;
                case 5004:
                   $scope.allow5004 = false;
                   break;
                 case 5005:
                   $scope.allow5005 = false;
                   break;
                case 5006:
                   $scope.allow5006 = false;
                   break;
                case 5007:
                   $scope.allow5007 = false;
                   break;
           }
            Flash.create('danger', 'You have already added this namespace to deny list.', 'formMessage');
            }
        }

        $scope.checkFuncDeny = function(id){
            var found = $scope.selection_allow.indexOf(id);
            if(found != -1){
              
            switch (id){
                case 5000:
                   $scope.deny5000 = false;
                   break;
                case 5001:
                   $scope.deny5001 = false;
                   break;
                case 5002:
                   $scope.deny5002 = false;
                   break;
                case 5003:
                   $scope.deny5003 = false;
                   break;
                case 5004:
                   $scope.deny5004 = false;
                   break;
                 case 5005:
                   $scope.deny5005 = false;
                   break;
                case 5006:
                   $scope.deny5006 = false;
                   break;
                case 5007:
                   $scope.deny5007 = false;
                   break;
           }
            }
             Flash.create('danger', 'You have already added this namespace to allow list.', 'formMessage');
        }


          $scope.selectedNamespacesAllow = function selectedNamespacesAllow() {
            return filterFilter($scope.namespaces_allow, { selected: true });
          };

          $scope.$watch('namespaces_allow|filter:{selected:true}', function (nv) {
            $scope.selection_allow = nv.map(function (namespace_allow) {
              return namespace_allow.value;
            });
          }, true);

          $scope.selectedNamespacesDeny = function selectedNamespacesDeny() {
            return filterFilter($scope.namespaces_deny, { selected: true });
          };

          $scope.$watch('namespaces_deny|filter:{selected:true}', function (nv) {
            $scope.selection_deny = nv.map(function (namespace_deny) {
              return namespace_deny.value;
            });
          }, true);


        /** Check if the item is already in the opposite array */
        function showdetails(item_id, search_array) {
             var found = $filter('filter')(search_array, {id: item_id}, true);
             if (found.length) 
              $scope.selected = "selected";
        }

         $scope.onSelectPages = function ($item) {
            showdetails($item.id, $scope.page_items_deny);
            if($scope.selected!=''){
                $scope.selected='';
                Flash.create('danger', 'You have already added this page to deny list.', 'formMessage');
                return true;
            }
            
            $scope.page_items.push({'id': $item.id,'name':$item.title});
            $scope.page_items_array.push($item.id);

            angular.forEach($scope.pages, function(value, key){
                    if(value.id === $item.id) {
                        $scope.pages.splice(key, 1);
                        return false;
                    }
            });
            $scope.tempProduct = null; //-- clear it
            $scope.pages_selected[$scope.pages_selected.length] = $item.id;
        };

        $scope.onSelectPagesDeny = function ($item) {
            showdetails($item.id, $scope.page_items);
            if($scope.selected!=''){
                $scope.selected='';
                Flash.create('danger', 'You have already added this page to allow list.', 'formMessage');
                return true;
            }
            $scope.page_items_deny.push({'id': $item.id,'name':$item.title});
            $scope.page_items_array_deny.push($item.id);

            angular.forEach($scope.pages_deny, function(value, key){
                    if(value.id === $item.id) {
                        $scope.pages_deny.splice(key, 1);
                        return false;
                    }
            });
            $scope.tempProductDeny = null; //-- clear it
            $scope.pages_selected_deny[$scope.pages_selected_deny.length] = $item.id;
        };



        $scope.onSelectCategories = function($item) {
            showdetails($item.id, $scope.category_items_deny);
            if($scope.selected!=''){
                $scope.selected='';
                Flash.create('danger', 'You have already added this category to deny list.', 'formMessage');
                return true;
            }

            $scope.category_items.push({'id':$item.id, 'name':$item.cat_title});
            $scope.category_items_array.push($item.id);

            
            angular.forEach($scope.categories, function(value, key){
                if(value.id === $item.id) {
                    $scope.categories.splice(key, 1);
                    return false;
                }
            });


            $scope.tempProductCat = null; //-- clear it
            $scope.categories_selected[$scope.categories_selected.length] = $item.id;
        };


        $scope.onSelectCategoriesDeny = function($item) {
            showdetails($item.id, $scope.category_items);
            if($scope.selected!=''){
                $scope.selected='';
                Flash.create('danger', 'You have already added this category to allow list.', 'formMessage');
                return true;
            }

            $scope.category_items_deny.push({'id':$item.id, 'name':$item.cat_title});
            $scope.category_items_array_deny.push($item.id);

            angular.forEach($scope.categories_deny, function(value, key){
                if(value.id === $item.id) {
                    $scope.categories_deny.splice(key, 1);
                    return false;
                }
            });


            $scope.tempProductCatDeny = null; //-- clear it
            $scope.categories_selected_deny[$scope.categories_selected_deny.length] = $item.id;
        };

        $scope.removeItem = function($index, $item, $type) {
            if($type === 'page') {
                $scope.page_items.splice($index, 1);
                $scope.page_items_array.splice($index, 1);
                $scope.pages.push({'id': $item.id, 'title':$item.name});
            }else if($type === 'category') {
                $scope.category_items.splice($index, 1);
                $scope.category_items_array.splice($index, 1);
                $scope.categories.push({'id': $item.id, 'cat_title':$item.name});
            }

        };
        $scope.removeItemDeny = function($index, $item, $type) {
            if($type === 'page') {
                $scope.page_items_deny.splice($index, 1);
                $scope.page_items_array_deny.splice($index, 1);
                $scope.pages_deny.push({'id': $item.id, 'title':$item.name});
            }else if($type === 'category') {
                $scope.category_items_deny.splice($index, 1);
                $scope.category_items_array_deny.splice($index, 1);
                $scope.categories_deny.push({'id': $item.id, 'cat_title':$item.name});
            }

        };

        $scope.savePermissionsAllow = function(){
           for (var i = 0; i <   $scope.category_items_array.length; ++i)
             $scope.category_items_object[i] =  $scope.category_items_array[i];
              for (var i = 0; i <   $scope.page_items_array.length; ++i)
             $scope.page_items_object[i] =  $scope.page_items_array[i];
             

             $scope.permissions.user_id =  $scope.user_id
             $scope.permissions.namespacesallow = $scope.selection_allow;
             $scope.permissions.categoryallow = $scope.category_items_object;
             $scope.permissions.pageallow     = $scope.page_items_object;
            ApiService.post(config.api.url + '/' + 'admin/set_permissions',  $scope.permissions ).then(function (data, status, headers, config) {
                Flash.create('success', 'Successfully Changed Permissions', 'formMessage');
                $scope.selection_allow=[];
                $scope.category_items=[];
                $scope.category_items_array=[];
                $scope.category_items_object={};
                $scope.page_items=[];
                $scope.page_items_array=[];
                $scope.page_items_object={};
            }, function (data, status, headers, config) {
            });
        }

         $scope.savePermissionsDeny = function(){
          for (var i = 0; i <   $scope.category_items_array_deny.length; ++i)
             $scope.category_items_object_deny[i] =  $scope.category_items_array_deny[i];
              for (var i = 0; i <   $scope.page_items_array_deny.length; ++i)
             $scope.page_items_object_deny[i] =  $scope.page_items_array_deny[i];

             $scope.permissions.user_id =  $scope.user_id
             $scope.permissions.namespacesdeny = $scope.selection_deny;
             $scope.permissions.categorydeny  = $scope.category_items_object_deny;
             $scope.permissions.pagedeny      = $scope.page_items_object_deny;
            ApiService.post(config.api.url + '/' + 'admin/set_permissions',  $scope.permissions ).then(function (data, status, headers, config) {
             Flash.create('success', 'Successfully Changed Permissions', 'formMessage');
                $scope.selection_deny=[];
                $scope.category_items_deny=[];
                $scope.category_items_array_deny=[];
                $scope.category_items_object_deny={};
                $scope.page_items_deny=[];
                $scope.page_items_array_deny=[];
                $scope.page_items_object_deny={};
            }, function (data, status, headers, config) {
            });
        }



        init();
       $scope.$watch('user_id', function (new_val) {
           if(new_val != null){
                $scope.getUserById(new_val);
                $scope.getAllCategories();
                $scope.getAllCategoriesDeny();
                $scope.getAllPages();
                $scope.getAllPagesDeny();
            }
        });


    });
