/**
 * Created by Miroslav Marinov on 30.09.2015.
 */

'use strict';

/**
 * @ngdoc function
 * @name adminLeadApp.controller:SocialMediaNotationsController
 * @description
 * # SocialMediaNotationsController
 * Controller of the adminLeadApp

 */

app.controller('SocialMediaNotationsController', function ($scope, ApiService, config, $location, $filter, $translate, Upload, Flash) {
    $scope.socialMediaNotations = [];
    $scope.singleSocialMediaNotation = {};
    $scope.front_url = config.front_url;
    $scope.pageLoading = false;
    $scope.InnerPageTitle = null;
    $scope.socialMediaNotationsObjectCategories = [];
    $scope.file = null;

    /**
     * Init Social Media Notations List Default data
     */
    var init = function () {
        //get path
        var path = $location.path().split("/");

        if (path[path.length - 1] == 'add') {
            $scope.InnerPageTitle = $translate.instant('add_social_media_notation');
            $scope.singleSocialMediaNotation = {};
            getSocialMediaNotationsObjectCategories();
        } else if (path[path.length - 2] == 'edit') {
            $scope.InnerPageTitle = $translate.instant('edit_media_notation');
            getSocialMediaNotationsObjectCategories();
        } else {
            $scope.InnerPageTitle = null;
            getSocialMediaNotations();
        }
    };

    /**
     * Get Social Media Notations List
     */
    var getSocialMediaNotations = function () {
        $scope.pageLoading = true;
        ApiService.get(config.api.url + '/' + 'admin/get_social_media_notations').then(function (data) {
            $scope.socialMediaNotations = data;
            $scope.pageLoading = false;
        }, function (data, status) {
            if (status == 401) {
                window.location.href = '/';
            }
        });
    };

    /**
     * Get Social Notation By ID
     * @param integer integer social_media_notation_id
     */
    var getSocialMediaNotationById = function (social_media_notation_id) {
        $scope.pageLoading = true;
        ApiService.get(config.api.url + '/' + 'admin/get_social_media_notation_by_id/' + social_media_notation_id).then(function (data) {
            $scope.singleSocialMediaNotation = data;
            $scope.pageLoading = false;
        }, function (data, status, headers, config) {
            if (status == 401) {
                window.location.href = '/';
            }
        });
    };

    /**
     * Get Object categories related to Social MediaNotations
     */
    var getSocialMediaNotationsObjectCategories = function () {
        ApiService.get(config.api.url + '/' + 'admin/get_social_media_notations_object_categories').then(function (data) {
            $scope.socialMediaNotationsObjectCategories = data;
            //after geting Object Categories and id is edit method call
            var path = $location.path().split("/");
            if (path[path.length - 2] == 'edit') {
                getSocialMediaNotationById(path[path.length - 1]);
            }
        }, function (data, status) {
            if (status == 401) {
                window.location.href = '/';
            }
        });
    };
    /**
     * Add Social Media Notation
     */
    var addSocialMediaNotation = function () {

        Upload.upload({
            url: config.api.url + '/' + 'admin/save_social_media_notation',
            fields: $scope.singleSocialMediaNotation,
            file: $scope.file
        }).success(function (data) {
            if (typeof data.success != 'undefined') {
                Flash.create('success', $translate.instant('social_media_notation_saved'), 'formMessage');
                window.location.href = '#/social_media_notations';
            }
            if (typeof data.error != 'undefined' && data.error == 'social_media_notation_upload_error') {
                Flash.create('danger', $translate.instant('social_media_notation_upload_error'), 'formMessage');
            }
            if (typeof data.error != 'undefined' && data.error == 'social_media_notation_not_saved') {
                Flash.create('danger', $translate.instant('social_media_notation_not_saved_edit'), 'formMessage');
            }
        }).error(function (data) {
            Flash.create('danger', $translate.instant('social_media_notation_upload_error'), 'formMessage');
        });
    };

    /**
     * Edit Social Media Notation
     */
    var editEditNotation = function () {

        if (typeof $scope.file == 'undefined') {
            ApiService.post(config.api.url + '/' + 'admin/save_social_media_notation', $scope.singleSocialMediaNotation).then(function (data) {
                if (typeof data.success != 'undefined') {
                    Flash.create('success', $translate.instant('social_media_notation_saved_edit'), 'formMessage');
                    window.location.href = '#/social_media_notations';
                }
                if (typeof data.error != 'undefined' && data.error == 'social_media_notation_upload_error') {
                    Flash.create('danger', $translate.instant('social_medial_notation_upload_error'), 'formMessage');
                }
                if (typeof data.error != 'undefined' && data.error == 'social_media_notation_not_saved_edit') {
                    Flash.create('danger', $translate.instant('social_media_notation_not_saved_edit'), 'formMessage');
                }
            }, function (data) {

                window.location.href = '/';
            });
        } else {
            Upload.upload({
                url: config.api.url + '/' + 'admin/save_social_media_notation',
                fields: $scope.singleSocialMediaNotation,
                file: $scope.file
            }).success(function (data) {
                if (typeof data.success != 'undefined') {
                    Flash.create('success', $translate.instant('social_media_notation_saved_edit'), 'formMessage');
                    window.location.href = '#/social_media_notations';
                }
                if (typeof data.error != 'undefined' && data.error == 'social_media_notation_upload_error') {
                    Flash.create('danger', $translate.instant('social_medial_notation_upload_error'), 'formMessage');
                }
                if (typeof data.error != 'undefined' && data.error == 'social_media_notation_not_saved_edit') {
                    Flash.create('danger', $translate.instant('social_media_notation_not_saved_edit'), 'formMessage');
                }
            }).error(function (data) {
                Flash.create('danger', $translate.instant('social_media_notation_upload_error'), 'formMessage');
            });
        }

    };

    /**
     * Save (add/edit) Social Media Notation
     */
    $scope.save = function () {
        if (typeof $scope.singleSocialMediaNotation.id == 'undefined') {
            addSocialMediaNotation();
        } else {
            editEditNotation();
        }
    };

    //Init Default Data
    init();
});
