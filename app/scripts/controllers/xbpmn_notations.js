/**
 * Created by Miroslav Marinov on 28.09.2015.
 */
'use strict';

/**
 * @ngdoc function
 * @name adminLeadApp.controller:XbpmnNotationsController
 * @description
 * # XbpmnNotationsController
 * Controller of the adminLeadApp

 */

app.controller('XbpmnNotationsController', function ($scope, ApiService, config, $location, $filter, $translate, Upload, Flash) {
    $scope.xbpmnNotations = [];
    $scope.singleXbpmnNotation = {};
    $scope.front_url = config.front_url;
    $scope.pageLoading = false;
    $scope.InnerPageTitle = null;
    $scope.xbpmnNotationsObjectCategories = [];
    /**
     * Init X-BPMN Notations List Default data
     */
    var init = function () {
        //get path
        var path = $location.path().split("/");

        if (path[path.length - 1] == 'add') {
            $scope.InnerPageTitle = $translate.instant('add_xbpmn_notation');
            $scope.singleXbpmnNotation = {};
            getXbpmnNotationsObjectCategories();
        } else if (path[path.length - 2] == 'edit') {
            $scope.InnerPageTitle = $translate.instant('edit_xbpmn_notation');
            getXbpmnNotationsObjectCategories();
        } else {
            $scope.InnerPageTitle = null;
            getXbpmnNotation();
        }
    };

    /**
     * Get Meta Objects List
     */
    var getXbpmnNotation = function () {
        $scope.pageLoading = true;
        ApiService.get(config.api.url + '/' + 'admin/get_xbpmn_notations').then(function (data, status, headers, config) {
            $scope.xbpmnNotations = data;
            $scope.pageLoading = false;
        }, function (data, status, headers, config) {
            if (status == 401) {
                window.location.href = '/';
            }
        });
    };

    /**
     * Get X-BPMN Notation By ID
     * @param integer integer xbpmn_media_notation_id
     */
    var getXbpmnNotationById = function (xbpmn_media_notation_id) {
        $scope.pageLoading = true;
        ApiService.get(config.api.url + '/' + 'admin/get_xbpmn_notation_by_id/' + xbpmn_media_notation_id).then(function (data) {
            $scope.singleXbpmnNotation = data;
            $scope.pageLoading = false;
        }, function (data, status, headers, config) {
            if (status == 401) {
                window.location.href = '/';
            }
        });
    };
    /**
     * Get Object categories related to X-BPMN Notation
     */
    var getXbpmnNotationsObjectCategories = function () {
        ApiService.get(config.api.url + '/' + 'admin/get_xbpmn_notations_object_categories').then(function (data, status, headers, config) {
            $scope.xbpmnNotationsObjectCategories = data;
            //after geting Object Categories and id is edit method call
            var path = $location.path().split("/");
            if (path[path.length - 2] == 'edit') {
                getXbpmnNotationById(path[path.length - 1]);
            }
        }, function (data, status, headers, config) {
            if (status == 401) {
                window.location.href = '/';
            }
        });
    };



    var addXbpmnNotation = function () {

        Upload.upload({
            url: config.api.url + '/' + 'admin/add_xbpmn_notation',
            fields: $scope.singleXbpmnNotation,
            file: $scope.file
        }).success(function (data, status, headers, config) {
            if (typeof data.success != 'undefined') {
                Flash.create('success', $translate.instant('xbpmn_notation_success_added'), 'formMessage');
                window.location.href = '#/xbpmn_notations';
            }
            if (typeof data.error != 'undefined' && data.error == 'xbpmn_notation_upload_error') {
                Flash.create('danger', $translate.instant('xbpmn_notation_upload_error'), 'formMessage');
            }
            if (typeof data.error != 'undefined' && data.error == 'xbpmn_notation_not_added') {
                Flash.create('danger', $translate.instant('xbpmn_notation_adding_error'), 'formMessage');
            }
        }).error(function (data, status, headers, config) {
            Flash.create('danger', $translate.instant('xbpmn_notation_upload_error'), 'formMessage');
        });
    };

    var editXbpmnNotation = function () {

        if (typeof $scope.file == 'undefined') {
            ApiService.post(config.api.url + '/' + 'admin/edit_xbpmn_notation', $scope.singleXbpmnNotation).then(function (data, status, headers, config) {
                if (typeof data.success != 'undefined') {
                    Flash.create('success', $translate.instant('xbpmn_notation_success_updated'), 'formMessage');
                    window.location.href = '#/xbpmn_notations';
                }
                if (typeof data.error != 'undefined' && data.error == 'xbpmn_notation_notation_upload_error') {
                    Flash.create('danger', $translate.instant('xbpmn_notation_upload_error'), 'formMessage');
                }
                if (typeof data.error != 'undefined' && data.error == 'xbpmn_notation_not_updated') {
                    Flash.create('danger', $translate.instant('xbpmn_notation_update_error'), 'formMessage');
                }
            }, function (data, status, headers, config) {
                window.location.href = '/';
            });
        } else {

            Upload.upload({
                url: config.api.url + '/' + 'admin/edit_xbpmn_notation',
                fields: $scope.singleXbpmnNotation,
                file: $scope.file
            }).success(function (data, status, headers, config) {
                if (typeof data.success != 'undefined') {
                    Flash.create('success', $translate.instant('xbpmn_notation_success_updated'), 'formMessage');
                    window.location.href = '#/xbpmn_notations';
                }
                if (typeof data.error != 'undefined' && data.error == 'xbpmn_notation_notation_upload_error') {
                    Flash.create('danger', $translate.instant('xbpmn_notation_upload_error'), 'formMessage');
                }
                if (typeof data.error != 'undefined' && data.error == 'xbpmn_notation_not_updated') {
                    Flash.create('danger', $translate.instant('xbpmn_notation_update_error'), 'formMessage');
                }
            }).error(function (data, status, headers, config) {
                Flash.create('danger', $translate.instant('xbpmn_notation_upload_error'), 'formMessage');
            });
        }

    };

    $scope.save = function () {
        if (typeof $scope.singleXbpmnNotation.id == 'undefined') {
            addXbpmnNotation();
        } else {
            editXbpmnNotation();
        }

    };

    //Init Default Data
    init();
});